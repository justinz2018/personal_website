if(Meteor.isClient) {
	var updatePreview = function() {
		var title =  $("#insertForm input[name='title']").val();
		var previewContent = $("#insertForm textarea[name='previewContent']").val();
		var bodyContent = "";
		if($("#bodyContentField").is(":visible")) {
			bodyContent = $("#insertForm textarea[name='bodyContent']").val();
		}
		var createdBy = Meteor.user().username;
		var createdAt = $("#insertForm").data("createdat");
		if(!createdAt) createdAt = Date.now();
		var editedAt = null;
		if($("#insertForm").data("editmethod")) editedAt = Date.now();
		$("#previewWrapper").html("");
		var isMarkdown = Session.get("isMarkdown");
		if((title+previewContent+bodyContent).length > 0) {
				Blaze.renderWithData(Template.entryContent, { title: title, previewContent: previewContent, bodyContent: bodyContent,
												createdBy: createdBy, createdAt: createdAt, preview: "1", editedAt: editedAt, isMarkdown: isMarkdown, }, $("#previewWrapper")[0]);
		}
	}
	Template.insertEntryContent.events({
		"submit form": function(e) {
			e.preventDefault();
			var form = e.target;
			var method = form.method.value;
			var id;
			if(form.getAttribute("data-editmethod")) {
				method = form.getAttribute("data-editmethod");
				id = form.getAttribute("data-id");
			}
			var bodyContent = null;
			var isMarkdown = Session.get("isMarkdown");
			if(!isMarkdown) isMarkdown = false;
			if(form.bodyContent) bodyContent = form.bodyContent.value;
			Meteor.call(method, id, isMarkdown, form.title.value, form.previewContent.value, bodyContent, function(error) {
				if(error) {
					alert(error);
				} else {
					if(form.method && form.method.options && form.method.selectIndex && form.method.options[form.method.selectedIndex].getAttribute("data-onsuccess")) {
						Router.go(form.method.options[form.method.selectedIndex].getAttribute("data-onsuccess"));
					} else {
						Router.go("index");
					}
				}
			});
		},
		"click #entryType > li": function(e) {
			$("#entryType > li").removeClass("active");
			$(e.target).parent().addClass("active");
			Session.set("isMarkdown", $(e.target).text()=="Markdown");
			updatePreview();
		},
		"change #methodSelect": function(e) {
			if(e.currentTarget.value === "insertEntry_Home") {
				$("#bodyContentField").hide();
			} else {
				$("#bodyContentField").show();
			}
			updatePreview();
		},
		"keyup": function(e) {
			e.preventDefault();
			updatePreview();
		},
	});
	Template.insertEntryContent.helpers({
		isMarkdown: function() {
			return Session.get("isMarkdown");
		},
	});
	Template.insertEntryContent.onRendered(function() {
		if("isMarkdown" in Template.parentData(0)) {
			Session.set("isMarkdown", Template.parentData(0).isMarkdown);
		} else {
			Session.set("isMarkdown", true);
		}
		updatePreview();
	});
}
Router.route("/admin/insert", {
	name: "insertEntry",
	template: "insertEntry",
	data: function() {
		return { methods : [ { method:"insertEntry_Blog", onSuccess:"blog" }, { method:"insertEntry_Home", onSuccess:"index" },  ] };
	},
	onBeforeAction: function() {
		var user = Meteor.user();
		if(!user || !Roles.userIsInRole(user, ["admin", "canCreateEntry"])) {
			this.render("404");
			return;
		}
		this.next();
	}
});
Router.route("/admin/edit/blog/:_id", {
	name: "editEntry_Blog",
	template: "insertEntry",
	data: function() {
		return $.extend({}, Posts.findOne({ _id: this.params._id }), { editMethod: "insertEntry_Blog"});
	},
	onBeforeAction: function() {
		var user = Meteor.user();
		if(!user || !Roles.userIsInRole(user, ["admin"])) {
			this.render("404");
			return;
		}
		this.next();
	}
});
Router.route("/admin/edit/home/:_id", {
	name: "editEntry_Home",
	template: "insertEntry",
	data: function() {
		return $.extend({}, Home.findOne({ _id: this.params._id }), { editMethod: "insertEntry_Home"});
	},
	onBeforeAction: function() {
		var user = Meteor.user();
		if(!user || !Roles.userIsInRole(user, ["admin"])) {
			this.render("404");
			return;
		}
		this.next();
	}
});
