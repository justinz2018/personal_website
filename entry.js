if(Meteor.isClient) {
	Template.entry.events({
		"click .removeButton": function(event) {
			$("#confirmModal").data("id", $(event.currentTarget).data("id"));
		},
	});
	Template.entryContent.events({
		"click .removeButton": function(event) {
			$("#confirmModal").data("id", $(event.currentTarget).data("id"));
		},
	});
}
