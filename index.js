Home = new Mongo.Collection("home");


Router.route("/", {
	name: "index",
	template: "index",
});

if(Meteor.isClient) {
    Meteor.subscribe("home");
    Template.indexContent.helpers({
        entries: function() {
            return Home.find({}, { sort: { createdAt: -1 }, fields: { title: 1, previewContent: 1, createdAt: 1, createdBy: 1, editedAt: 1, isMarkdown: 1, } });
        },
    });
}


if (Meteor.isServer) {
	Meteor.startup(function () {
		Home._ensureIndex({ createdAt: 1 });
	});
	Meteor.publish("home", function() {
		return Home.find({});
	});
}

Meteor.methods({
	insertEntry_Home: function(id, isMarkdown, title, previewContent) {
		check(title, String);
		check(previewContent, String);
		check(isMarkdown, Boolean);
		id = id || "";
		check(id, String);
		var user = Meteor.user();
		if(user && Roles.userIsInRole(user, ["admin"])) {
			var ins = { title: title, previewContent: previewContent, createdBy: user.username, isMarkdown: isMarkdown };
			var t = Home.findOne({ _id: id });
			if(t) {
				ins["editedAt"] = Date.now();
				Home.update({ _id: id, }, { $set: ins });
			} else {
				ins["createdAt"] = Date.now();
				Home.insert(ins);
			}
		} else {
			throw new Meteor.Error(403, "not-authorized");
		}
	},
	removeEntry_Home: function(id) {
		check(id, String);
		var user = Meteor.user();
		if(user && Roles.userIsInRole(user, ["admin"])) {
			var num = Home.remove({ _id: id });
			if(num <= 0) {
				throw new Meteor.Error(404, "id-not-found");
			}
		} else {
			throw new Meteor.Error(403, "not-authorized");
		}
	}
});
