
Router.configure({
//	loadingTemplate: "index",
	notFoundTemplate: "404",
});
Router.plugin("dataNotFound", { notFoundTemplate: "404" });

if (Meteor.isClient) {
	jQuery.fn.rotate = function(deg) {
		$(this).css("transform", "rotate("+deg+"deg)");
		return $(this);
	};
	Template.main.events({
		"click .clickable": function(event) {
			var url = $(event.currentTarget).data("url");
			if(url) {
				Router.go(url);
			}
		},
		"click #loginDropdownPanel": function() {
			$("#loginDropdown").slideToggle(400);
			if($("#loginDropdown").data("down") === "1") {
				$("#loginDropdown").data("down", "0");
				var rot = 90;
				var loop = setInterval(function() {
					if(rot <= 0) clearInterval(loop);
					else {
						$("#loginDropdownPanel i.fa-arrow-circle-right").rotate(rot);
						rot -= 5;
					}
				}, 8);
			} else {
				$("#loginDropdown").data("down", "1");
				var rot = 0;
				var loop = setInterval(function() {
					if(rot >= 90) clearInterval(loop);
					else {
						$("#loginDropdownPanel i.fa-arrow-circle-right").rotate(rot);
						rot += 5;
					}
				}, 8);
			}
		},
		"click #logout": function() {
			AccountsTemplates.logout();
		}
	});
	Template.confirmModal.events({
		"click #confirmModalSubmit": function() {
			Meteor.call($("#confirmModal").data("method"), $("#confirmModal").data("id"), function(error) {
				if(error) alert(error);
				else {
					$("body").removeClass("modal-open");
					$(".modal-backdrop").remove();
					if($("#confirmModal").data("redirect")) {
						Router.go($("#confirmModal").data("redirect"));
					} else {
						Router.go("index");
					}
				}
			});
		},
	});
	Template.registerHelper("dateFormat", function(epoch) {
		epoch = epoch || Date.now();
		var date = new Date(epoch);
		var hrs = date.getHours()%12;
		if(hrs == 0) hrs = 12;
		var min = date.getMinutes();
		if(min < 10) min = "0"+min;
		return (date.getMonth()+1)+"-"+date.getDate()+"-"+date.getFullYear()+" "+hrs+":"+min+" "+(date.getHours()<12?"AM":"PM");
	});
	Template.registerHelper("extend", function(dict) {
		return $.extend({}, this, dict.hash);
	});
}

if (Meteor.isServer) {
	Meteor.startup(function () {
	});
	validLong = Match.Where(function(num) {
		return !isNan(parseInt(num)) && isFinite(num);
	});
    Meteor.methods({
        "userExists": function(username){
			check(username, String);
            return !!Meteor.users.findOne({username: username});
        },
    });
}
AccountsTemplates.removeField("email");
AccountsTemplates.addField({
    _id: "username",
    type: "text",
    required: true,
    func: function(value) {
        if (Meteor.isClient) {
            var self = this;
			self.setValidating(true);
            Meteor.call("userExists", value, function(error, userExists) {
                if (!userExists) {
                    self.setSuccess();
                } else {
					alert("Username already taken");	//Wait for fix: https://github.com/meteor-useraccounts/core/issues/626
                    self.setError(true);
                }
				self.setValidating(false);
            });
            return;
        }
        return Meteor.call("userExists", value);
    },
	errStr: "Username already taken",
});
AccountsTemplates.addField(AccountsTemplates.removeField("password"));
