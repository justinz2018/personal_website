Posts = new Mongo.Collection("posts");


Router.route("/blog", {
	name: "blog",
	template: "blog",
});

Router.route("/blog/:_id", {
	name: "blogPost",
	template: "blogPost",
	data: function() {
		return Posts.findOne({ _id: this.params._id });
	},
	waitOn: function() {
		return Meteor.subscribe("posts");
	},
});

if (Meteor.isClient) {
	Meteor.subscribe("posts");
	Template.blogContent.helpers({
		posts: function() {
			return Posts.find({}, { sort: { createdAt: -1 }, fields: { title: 1, previewContent: 1, createdAt: 1, createdBy: 1, editedAt: 1, isMarkdown: 1, } });
		},
	});
}
if (Meteor.isServer) {
	Meteor.startup(function () {
		Posts._ensureIndex({ createdAt: 1 });
	});
	Meteor.publish("posts", function() {
		return Posts.find({});
	});
}

Meteor.methods({
	insertEntry_Blog: function(id, isMarkdown, title, previewContent, bodyContent) {
		check(title, String);
		check(previewContent, String);
		check(bodyContent, String);
		check(isMarkdown, Boolean);
		id = id || "";
		check(id, String);
		var user = Meteor.user();
		if(user && Roles.userIsInRole(user, ["admin"])) {
			var ins = { title: title, previewContent: previewContent, bodyContent: bodyContent, createdBy: user.username, isMarkdown: isMarkdown };
			var t = Posts.findOne({ _id: id });
			if(t) {
				ins["editedAt"] = Date.now();
				Posts.update({ _id: id, }, { $set: ins });
			} else {
				ins["createdAt"] = Date.now();
				Posts.insert(ins);
			}
		} else {
			throw new Meteor.Error(403, "not-authorized");
		}
	},
	removeEntry_Blog: function(id) {
		check(id, String);
		var user = Meteor.user();
		if(user && Roles.userIsInRole(user, ["admin"])) {
			var num = Posts.remove({ _id: id });
			if(num <= 0) {
				throw new Meteor.Error(404, "id-not-found");
			}
		} else {
			throw new Meteor.Error(403, "not-authorized");
		}
	}
});
